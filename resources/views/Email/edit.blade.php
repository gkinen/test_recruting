@extends('layouts.admin')
@section('content')
@include('alerts.request')
	{!!Form::model($email,['route'=>['email.update', $email],'method'=>'PUT', 'files' => true])!!}
		@include('email.forms.email')
		{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}
	{!!Form::close()!!}
@endsection