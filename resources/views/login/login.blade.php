@extends('layouts.login')
@section('content')
@include('alerts.errors')
{!!Form::open(['route'=>'login.authenticated', 'method'=>'POST'])!!}
	@include('login.forms.login')
	{!!Form::submit('Ingresar',['class'=>'btn btn-primary'])!!}
{!!Form::close()!!}
@endsection

<!-- Laravel Collective : Definir el objeto Form -->