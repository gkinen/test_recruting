<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'EmailController@index');

Route::get('email/{id}/delete', [
    'as' => 'email.delete',
    'uses' => 'EmailController@delete',
]);

Route::get('email/{id}/edit', [
    'as' => 'email.edit',
    'uses' => 'EmailController@edit',
]);


Route::put('email/{id}/update', [
    'as' => 'email.update',
    'uses' => 'EmailController@update',
]);

Route::get('email/create', [
    'as' => 'email.create',
    'uses' => 'EmailController@create',
]);

Route::post('email/store', [
    'as' => 'email.store',
    'uses' => 'EmailController@store',
]);

Route::get('/login', function () {return view('login/login');}, [ 'as' => 'login']);

Route::post('login/login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@login',
]);

Route::post('login/authenticated', [
    'as' => 'login.authenticated',
    'uses' => 'Auth\LoginController@authenticated',
]);

Route::get('/send/emails', 'EmailController@send_emails');

Route::get('login/logout',[ 
    'as' => 'logout',
    'uses' => '\App\Http\Controllers\Auth\LoginController@logout'
]);