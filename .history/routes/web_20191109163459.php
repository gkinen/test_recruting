<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'EmailController@index');

Route::get('email/{id}/delete', [
    'as' => 'email.delete',
    'uses' => 'EmailController@delete',
]);

Route::get('email/{id}/edit', [
    'as' => 'email.edit',
    'uses' => 'EmailController@edit',
]);