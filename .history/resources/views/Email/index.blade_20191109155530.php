@extends('layouts.admin')
	@include('alerts.success')
	@section('content')
	<br>
		<table id="tabla" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Destinatario</th>
                <th>Asunto</th>
                <th>Fecha/Hora envío</th>
                <th>Operación</th>
            </tr>
        </thead>
        <tbody>
            @foreach($emails as $email)
            <tr>
				<td>{{$email->id}}</td>
				<td>{{$email->nombre}}</td>
				<td>{{$email->descripcion}}</td>
				<td>
                <a href="{{ url('email.edit') }}">Add new user</a

				</td>
			</tr>
			@endforeach
        </tbody>
    </table>
	@endsection
	
	@section('scripts')
	<script>
		$(document).ready(function() {
    		$('#tabla').DataTable();
		});
	</script>	
	@endsection