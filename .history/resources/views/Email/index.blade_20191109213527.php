@extends('layouts.admin')
	@include('alerts.success')
	@section('content')
	<br>
		<table id="tabla" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Destinatario</th>
                <th>Asunto</th>
                <th>Fecha/Hora envío</th>
                <th>Estado</th>
                <th>Operación</th>
            </tr>
        </thead>
        <tbody>
            @foreach($emails as $email)
            <tr>
				<td>{{$email->destinatario}}</td>
				<td>{{$email->asunto}}</td>
				<td>{{$email->fecha_hora_envio}}</td>
				<td>{{$email->estado->nombre}}</td>
				<td>
					{!!link_to_route('email.edit', $title = 'Editar', $parameters = $email, $attributes = ['class'=>'btn btn-primary'])!!}

					{!!link_to_route('email.delete', $title = 'Eliminar', $parameters = $email->id, $attributes = ['class'=>'btn btn-danger', 'onclick'=>'return confirm("Esta seguro que quiere eliminar este email?")'])!!}

				</td>
			</tr>
			@endforeach
        </tbody>
    </table>
	@endsection
	
	@section('scripts')
	<script>
		$(document).ready(function() {
    		$('#tabla').DataTable();
		});
	</script>	
	@endsection