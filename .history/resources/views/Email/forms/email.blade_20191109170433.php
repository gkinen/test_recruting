<div class="form-group">
	{!!Form::label('Destinatario','destinatario:')!!}
	{!!Form::text('destinatario',null,['class'=>'form-control','placeholder'=>'Ingresa el destinatario del Email'])!!}
</div>
<div class="form-group">
	{!!Form::label('Asunto','asunto:')!!}
	{!!Form::text('asunto',null,['class'=>'form-control','placeholder'=>'Ingresa el asunto del Email'])!!}
</div>
<div class="form-group">
	{!!Form::label('descripcion','Descripcion:')!!}
	{!!Form::textarea('descripcion',null,['class'=>'form-control','placeholder'=>'Ingresa la Descripcion'])!!}
</div>