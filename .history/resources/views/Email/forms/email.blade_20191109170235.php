<div class="form-group">
	{!!Form::label('destinatario','destinatario:')!!}
	{!!Form::text('destinatario',null,['class'=>'form-control','placeholder'=>'Ingresa el destinatario del Articulo'])!!}
</div>
<div class="form-group">
	{!!Form::label('descripcion','Descripcion:')!!}
	{!!Form::textarea('descripcion',null,['class'=>'form-control','placeholder'=>'Ingresa la Descripcion'])!!}
</div>