<div class="form-group">
	{!!Form::label('Destinatario','destinatario:')!!}
	{!!Form::text('destinatario',null,['class'=>'form-control','placeholder'=>'Ingresa el destinatario del Email'])!!}
</div>
<div class="form-group">
	{!!Form::label('Asunto','asunto:')!!}
	{!!Form::text('asunto',null,['class'=>'form-control','placeholder'=>'Ingresa el asunto del Email'])!!}
</div>
<div class="form-group">
	{!!Form::label('cuerpo','Cuerpo del Email:')!!}
	{!!Form::textarea('cuerpo',null,['class'=>'form-control','placeholder'=>'Ingresa el cuerpo del Email'])!!}
</div>
<div class="form-group">
	{!!Form::label('adjunto','Adjunto:')!!}
	{!!Form::file('adjunto',null,['class'=>'form-control','placeholder'=>'Selecciona el archivo adjunto.'])!!}
</div>
<div class="form-group">
	{!!Form::label('fecha_hora_envio','Fecha Hora de envío:')!!}
	{!!Form::datetime('cuerpo',null,['class'=>'form-control','placeholder'=>'Seleccione la fecha y hora de envío.*'])!!}
</div>