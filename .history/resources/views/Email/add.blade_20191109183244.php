@extends('layouts.admin')
@section('content')
@include('alerts.request')
	{!!Form::model($email,['route'=>['email.store', $email],'method'=>'POST', 'files' => true])!!}
		@include('email.forms.email')
		{!!Form::submit('Agregar',['class'=>'btn btn-primary'])!!}
	{!!Form::close()!!}
@endsection