@extends('layouts.admin')
@section('content')
@include('alerts.request')
{!!Form::open(['route'=>'email.store', 'method'=>'POST'])!!}
	@include('email.forms.email')
	{!!Form::submit('Agregar',['class'=>'btn btn-primary'])!!}
{!!Form::close()!!}
@endsection

<!-- Laravel Collective : Definir el objeto Form -->