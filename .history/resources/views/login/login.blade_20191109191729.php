<div class="form-group">
	{!!Form::label('destinatario','Destinatario:')!!}
	{!!Form::text('destinatario',null,['class'=>'form-control','placeholder'=>'Ingresa el destinatario del Email'])!!}
</div>
<div class="form-group">
	{!!Form::label('asunto','Asunto:')!!}
	{!!Form::text('asunto',null,['class'=>'form-control','placeholder'=>'Ingresa el asunto del Email'])!!}
</div>
<div class="form-group">
	{!!Form::label('cuerpo','Cuerpo del Email:')!!}
	{!!Form::textarea('cuerpo',null,['class'=>'form-control','placeholder'=>'Ingresa el cuerpo del Email'])!!}
</div>
<div class="form-group">
	{!!Form::label('adjunto','Adjunto:')!!}
	{!!Form::file('adjunto',null,['class'=>'form-control','placeholder'=>'Selecciona el archivo adjunto.'])!!}
</div>
<div class="form-group">
	{!!Form::label('fecha_envio','Fecha de envío:')!!}
	{!!Form::date('fecha_envio',null,['class'=>'form-control','placeholder'=>'Seleccione la fecha de envío.*'])!!}
</div>
<div class="form-group">
	{!!Form::label('hora_envio','Hora de envío:')!!}
	{!!Form::time('hora_envio', null,['class'=>'form-control','placeholder'=>'Seleccione la hora de envío.*'])!!}
</div>