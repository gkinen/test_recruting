<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model{
    
    protected $table = 'estados'; 

      protected $fillable = ['nombre'];

      public function email() {
        return $this->hasMany(App\Email::class);
    }
}