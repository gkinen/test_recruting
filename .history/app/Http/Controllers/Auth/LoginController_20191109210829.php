<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Redirect;
use Auth;
use Session;
use App\user;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request)
    {
        
        //$credentials = [ 'email' => $request->email , 'password' => $request->password ];
        
        
        // if(Auth::attempt($credentials, $request->remember)){ // login attempt
            
        //     return Redirect::to('/');
        // } 
        
        $user = user::find([ 'email' => $request->email , 'password' => $request->password ]);

        if($user->count()){
            //Auth::loginUsingId($user->id);
            Auth::login($user->get(), true);
            return Redirect::to('/');
        }
        
        Session::flash('message-error' , 'Usuario y/o contraseña incorrectos');
        return Redirect::to('/login');
    }
}
