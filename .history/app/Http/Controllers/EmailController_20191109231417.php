<?php

namespace App\Http\Controllers;


use App\Http\Requests\emailRequest;
use Illuminate\Http\Request;
use App\email;

use Redirect;
use Session;

use PHPMailer\PHPMailer;

class emailController extends Controller{
	
	public function __construct(){
		$this->middleware('auth');
	}
	
    public function index(){

		$emails = email::all();
		
		$estados = array(1 => 'Pediente', 2 => 'Enviado', 3 => 'Fallido');

    	return view('email.index', compact('emails'), compact('estados'));
    }

    public function create(){

    	return view('email.create');
    }

    public function store(emailRequest $request){

		$email = new Email;

		//print_r($request->input('destinatario'));exit;
		
		$email->destinatario = $request->input('destinatario');
		$email->asunto = $request->input('asunto');
		$email->cuerpo = $request->input('cuerpo');
		$email->fecha_envio = $request->input('fecha_envio');
		$email->hora_envio = $request->input('hora_envio');
		$email->estado_id = '1';
		
		//guardamos el adjunto si se agrego
		if( $request->hasFile('adjunto') ) {
			$request->adjunto->storeAs('adjuntos', $request->adjunto->getClientOriginalName());
			$email->path_adjunto = 'storage/app/adjuntos/' . $request->adjunto->getClientOriginalName();
		}

    	// $email->fill($request->all());

    	$email->save();

    	Session::flash('message' , 'Email Creado Correctamente');

    	return Redirect::to('/');
    	email::create($request->all());

    	Session::flash('message' , 'email Creado Correctamente');

    	return Redirect::to('/email');
    }

    public function show($id){

    }

    public function edit($id){

    	$email = email::find($id);

    	return view('email.edit' , compact('email'));
    }

    public function update(emailRequest $request , $id){

		$email = email::find($id);
		
		$email->destinatario = $request->input('destinatario');
		$email->asunto = $request->input('asunto');
		$email->cuerpo = $request->input('cuerpo');
		$email->fecha_envio = $request->input('fecha_envio');
		$email->hora_envio = $request->input('hora_envio');
		
		//guardamos el adjunto si se agrego
		if( $request->hasFile('adjunto') ) {
			$request->adjunto->storeAs('adjuntos', $request->adjunto->getClientOriginalName());
			$email->path_adjunto = 'storage/app/adjuntos/' . $request->adjunto->getClientOriginalName();
		}

    	// $email->fill($request->all());

    	$email->save();

    	Session::flash('message' , 'Email Actualizado Correctamente');

    	return Redirect::to('/');
    }

    public function delete($id){

    	$email = email::find($id);

    	if($email != null){

    		$email->delete();

    		Session::flash('message' , 'Email Eliminado Correctamente');
    	}

    	return Redirect::to('/');
    }



	public function send_emails()
	{
		$emails = user::where(['estado_id' => 1]);
		
		
		$text             = 'Hello Mail';
        $mail             = new PHPMailer\PHPMailer(); // create a n
        $mail->SMTPDebug  = 1; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth   = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host       = "smtp.mailtrap.io";
        $mail->Port       = 25; // or 587
        $mail->IsHTML(false);
        $mail->Username = "cc61a256f3c1dd";
        $mail->Password = "5f1e1a0a7a898e";
        $mail->SetFrom("recruting@gmail.com", 'Recruting');

		foreach ($emails as $items) {			
			
			$mail->Subject = $items->asunto;
			$mail->Body    = $text;
			$mail->AddAddress("gnk1978@gmail.com", "Recruting");
			if ($mail->Send()) {
				return 'Email Sended Successfully';
			} else {
				return 'Failed to Send Email';
			}
		}
	}

}
