<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmailRequest;
use Illuminate\Http\Request;
use App\Email;

use Redirect;
use Session;

class EmailController extends Controller{
	
    public function index(){

    	$Emails = Email::all();

    	return view('Email.index', compact('Emails'));
    }

    public function create(){

    	return view('Email.create');
    }

    public function store(EmailRequest $request){

    	Email::create($request->all());

    	Session::flash('message' , 'Email Creado Correctamente');

    	return Redirect::to('/Email');
    }

    public function show($id){

    }

    public function edit($id){

    	$Email = Email::find($id);

    	return view('Email.edit' , compact('Email'));
    }

    public function update(EmailRequest $request , $id){

    	$Email = Email::find($id);

    	$Email->fill($request->all());

    	$Email->save();

    	Session::flash('message' , 'Email Actualizado Correctamente');

    	return Redirect::to('/Email');
    }

    public function destroy($id){

    	$Email = Email::find($id);

    	if($Email != null){

    		$Email->delete();

    		Session::flash('message' , 'Email Eliminado Correctamente');
    	}

    	return Redirect::to('/Email');
    }


}
