<?php

namespace App\Http\Controllers;

use App\Http\Requests\emailRequest;
use Illuminate\Http\Request;
use App\email;

use Redirect;
use Session;

class emailController extends Controller{
	
    public function index(){

    	$emails = email::all();

    	return view('email.index', compact('emails'));
    }

    public function create(){

    	return view('email.create');
    }

    public function store(emailRequest $request){

    	email::create($request->all());

    	Session::flash('message' , 'email Creado Correctamente');

    	return Redirect::to('/email');
    }

    public function show($id){

    }

    public function edit($id){

    	$email = email::find($id);

    	return view('email.edit' , compact('email'));
    }

    public function update(emailRequest $request , $id){
		print_r($request);exit;
    	$email = email::find($id);

    	$email->fill($request->all());

    	$email->save();

    	Session::flash('message' , 'Email Actualizado Correctamente');

    	return Redirect::to('/');
    }

    public function delete($id){

    	$email = email::find($id);

    	if($email != null){

    		$email->delete();

    		Session::flash('message' , 'email Eliminado Correctamente');
    	}

    	return Redirect::to('/email');
    }


}
