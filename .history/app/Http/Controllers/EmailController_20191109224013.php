<?php

namespace App\Http\Controllers;


use App\Http\Requests\emailRequest;
use Illuminate\Http\Request;
use App\email;

use Redirect;
use Session;

use Illuminate\Support\Facades\Mail;

use App\Mail\SendMailable;

class emailController extends Controller{
	
	public function __construct(){
		$this->middleware('auth');
	}
	
    public function index(){

		$emails = email::all();
		
		$estados = array(1 => 'Pediente', 2 => 'Enviado', 3 => 'Fallido');

    	return view('email.index', compact('emails'), compact('estados'));
    }

    public function create(){

    	return view('email.create');
    }

    public function store(emailRequest $request){

		$email = new Email;

		//print_r($request->input('destinatario'));exit;
		
		$email->destinatario = $request->input('destinatario');
		$email->asunto = $request->input('asunto');
		$email->cuerpo = $request->input('cuerpo');
		$email->fecha_envio = $request->input('fecha_envio');
		$email->hora_envio = $request->input('hora_envio');
		$email->estado_id = '1';
		
		//guardamos el adjunto si se agrego
		if( $request->hasFile('adjunto') ) {
			$request->adjunto->storeAs('adjuntos', $request->adjunto->getClientOriginalName());
			$email->path_adjunto = 'storage/app/adjuntos/' . $request->adjunto->getClientOriginalName();
		}

    	// $email->fill($request->all());

    	$email->save();

    	Session::flash('message' , 'Email Creado Correctamente');

    	return Redirect::to('/');
    	email::create($request->all());

    	Session::flash('message' , 'email Creado Correctamente');

    	return Redirect::to('/email');
    }

    public function show($id){

    }

    public function edit($id){

    	$email = email::find($id);

    	return view('email.edit' , compact('email'));
    }

    public function update(emailRequest $request , $id){

		$email = email::find($id);
		
		$email->destinatario = $request->input('destinatario');
		$email->asunto = $request->input('asunto');
		$email->cuerpo = $request->input('cuerpo');
		$email->fecha_envio = $request->input('fecha_envio');
		$email->hora_envio = $request->input('hora_envio');
		
		//guardamos el adjunto si se agrego
		if( $request->hasFile('adjunto') ) {
			$request->adjunto->storeAs('adjuntos', $request->adjunto->getClientOriginalName());
			$email->path_adjunto = 'storage/app/adjuntos/' . $request->adjunto->getClientOriginalName();
		}

    	// $email->fill($request->all());

    	$email->save();

    	Session::flash('message' , 'Email Actualizado Correctamente');

    	return Redirect::to('/');
    }

    public function delete($id){

    	$email = email::find($id);

    	if($email != null){

    		$email->delete();

    		Session::flash('message' , 'Email Eliminado Correctamente');
    	}

    	return Redirect::to('/');
    }

	public function send_emails()
	{
		$test = Mail::send([], [], function ($message) {
			$message->to('gnk1978@gmail.com')
			->subject('prueba')
			->setBody('Hi, welcome user!'); 
		});
		if($test){
			echo 'enviado';
		}else{
			echo 'no enviado';
		}
	}

}
