<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model{
    
    protected $table = 'emails'; 

      protected $fillable = ['destinatario', 'asunto'];

      public function estado() {
        return $this->hasOne(App\Estado::class);
    }
}